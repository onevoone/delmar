var app = angular.module('myApp', ['ui.bootstrap.modal', 'ui.bootstrap', 'ngSanitize']);

app.controller('contentCtrl', ['$scope', '$http', '$window', 'uibButtonConfig', function($scope, $http, $window, buttonConfig) {

	$(window).scroll(function () {
		if ($(this).scrollTop() > 700) {
			$('.gototop').fadeIn();
		} else {
			$('.gototop').fadeOut();
		}
	});
	$('.gototop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 400);
			return false;
		});


	$scope.place1 = '1 дополнительное место';
	$scope.place2 = '2 дополнительных места';

	window.onload = showViewport;
	window.onresize = showViewport;


function showViewport() {
	var width = Math.max(document.documentElement.clientWidth, window.innerWidth);
	if (width < 768) {
		// $scope.place1 = '1 доп. место';
		// $scope.place2 = '2 доп. места';

		document.getElementById("place1").innerHTML = '1 доп. место';
		document.getElementById("place2").innerHTML = '2 доп. места';

		document.getElementById("fio").setAttribute("placeholder", "ФИО");
		document.getElementById("fio").setAttribute("placeholder", "ФИО");
		document.getElementById("passport").setAttribute("placeholder", "Серия и номер паспорта");
		document.getElementById("phone").setAttribute("placeholder", "Контактный телефон");
		document.getElementById("email").setAttribute("placeholder", "E-mail");
		document.getElementById("comments").setAttribute("placeholder", "Примечание");

		console.log(width);
	}
}

	console.log(window.screen.width, window.screen.height);
	
	$scope.radioModel = 'null';

	$scope.radioClass1 = 'select-label';
	$scope.radioClass2 = 'select-label';

	$scope.change1 = function() {
		console.log($scope.radioModel);
		if(	$scope.radioClass1 === 'select-label' && $scope.radioClass2 === 'select-label' ||
				$scope.radioClass1 === 'select-label' && $scope.radioClass2 === 'selected-input' 
			) {
			$scope.radioClass1 = 'selected-input';
			$scope.radioClass2 = 'select-label';
		} else {
			$scope.radioClass1 = 'select-label';
			$scope.radioClass2 = 'select-label';
		}
	};
	
	$scope.change2 = function() {
		console.log($scope.radioModel);
		if(	$scope.radioClass1 === 'select-label' && $scope.radioClass2 === 'select-label' ||
				$scope.radioClass1 === 'selected-input' && $scope.radioClass2 === 'select-label' 
			) {
			$scope.radioClass2 = 'selected-input';
			$scope.radioClass1 = 'select-label';
		} else {
			$scope.radioClass1 = 'select-label';
			$scope.radioClass2 = 'select-label';
		}
	};

	$scope.months = [
			{
				id: 1,
				name: 'Январь'
			},
			{
				id: 2,
				name: 'Февраль'
			},
			{
				id: 3,
				name: 'Март'
			},
			{
				id: 4,
				name: 'Апрель'
			},
			{
				id: 5,
				name: 'Май'
			},
			{
				id: 6,
				name: 'Июнь'
			},
			{
				id: 7,
				name: 'Июль'
			},
			{
				id: 8,
				name: 'Август'
			},
			{
				id: 9,
				name: 'Сентябрь'
			},
			{
				id: 10,
				name: 'Октябрь'
			},
			{
				id: 11,
				name: 'Ноябрь'
			},
			{
				id: 12,
				name: 'Декабрь'
			},
	];


	$scope.payment = [
			{
				id: 1,
				name: 'Безналичный расчет(через банк)'
			},
			{
				id: 2,
				name: 'Предварительное'
			},
			{
				id: 3,
				name: 'Westernt union/UniStream и т.д.'
			},
			{
				id: 4,
				name: 'Другое, укажите в пимечании'
			}
	];



	$scope.submitForm = function() {

	


	};

	$scope.acceptOrder = function() {
		alert(123);
	};

}]);