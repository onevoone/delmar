var app = angular.module('myApp', ['ui.bootstrap.modal', 'ui.bootstrap', 'ngSanitize']);

app.controller('mainCtrl', ['$scope', '$http', '$window', 'uibButtonConfig', function($scope, $http, $window, buttonConfig) {

	$(window).scroll(function () {
		if ($(this).scrollTop() > 700) {
			$('.gototop').fadeIn();
		} else {
			$('.gototop').fadeOut();
		}
	});
	$('.gototop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 400);
			return false;
		});

}]);