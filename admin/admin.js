jQuery(document).ready(function($) {
	$('.icon-menu').click(function(event) {
		$('.sidebar').toggleClass('active');
	});

	$('.show-clients').click(function(event) {
		$('.clients-content').removeClass('hidden');
		$('.cottages-content').addClass('hidden');
	});

	$('.show-cottages').click(function(event) {
		$('.clients-content').addClass('hidden');
		$('.cottages-content').removeClass('hidden');
	});

	$('#myModal').on('shown.bs.modal', function () {
		$('#myInput').focus()
	})
});

jQuery(document).ready(function($) {
	$('#radio1, #radio2, #radio3').click(function(event) {
		$('.area').addClass('hidden');
	});
	$('#radio4').click(function(event) {
		$('.area').removeClass('hidden');
	});
});
